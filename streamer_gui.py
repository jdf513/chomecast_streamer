import tkinter as tk
import stream2chromecast as streamer

class streamer_gui():
    def __init__(self):
        self.top = tk.Tk()

        # Create list of files
        self.file_listbox = tk.Listbox(self.top)

        # Buttons
        add_button = tk.Button(self.top, text="Add music", command=self.add_music)
        play_button = tk.Button(self.top, text="Play", command=self.play_b)
        
        self.file_listbox.pack()
        add_button.pack()
        play_button.pack()

        self.file_list = []
        
    def add_music(self):
        filenames = tk.filedialog.askopenfilename(multiple=True)
        for i in filenames:
            self.file_listbox.insert(tk.END, i.split("/")[-1])
            self.file_list.append(i)

    def run(self):
        self.top.mainloop()

    def play_b(self):
        streamer.play(self.file_list[0])


if __name__ == "__main__":
    s = streamer_gui()
    s.run()
